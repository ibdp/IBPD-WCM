<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <form id="fm" class="forms" action="<%=basePath%>manage/paper/doAdd.do" method="post">
    	<input type="hidden" name="id" id="id" value=""/>
    	<table cellpadding="0" cellspacing="0" border="0">
    		<tr>
    			<th class="title">期刊/杂志名称</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="paperName" name="paperName"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">类别(期刊:1或杂志:2)</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="type" name="type"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">排序</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="order" name="order"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    		<tr>
    			<th class="title">发布目录(相对目录)</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="publishDir" name="publishDir"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
     		<tr>
    			<th class="title">字段说明</th>
    			<td class="val">
    				<input type="text" class="inputxt" id="intro" name="intro"/>
    			</td>
    			<td class="reg"><div class="Validform_checktip"></div></td>
    		</tr>
    	</table>
    	<input type="submit"/>&nbsp;&nbsp;<input type="button" value="关闭" onclick="javascript:$('#addDialog').window('close');"/>
    </form> 
    <script type="text/javascript">
    $(function(){
		
	$("#fm").Validform({
		tiptype:2,
		callback:function(form){
			var check=confirm("您确定要提交表单吗？");
			if(check){
				form[0].submit();
			}
			
			return false;
		}
		
	});
})
</script>