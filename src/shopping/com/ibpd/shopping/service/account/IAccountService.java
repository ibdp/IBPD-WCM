package com.ibpd.shopping.service.account;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.AccountEntity;

public interface IAccountService extends IBaseService<AccountEntity> {
	Boolean checkAccountExist(String account);
	Boolean checkNicknameExist(String nickname);
	Boolean checkEmailExist(String email);
	AccountEntity getAccountByAccount(String account);
	AccountEntity getAccountByEmail(String email);
	void initTestAccount();
	
}
