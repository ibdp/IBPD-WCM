package com.ibpd.shopping.service.cart;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.CartEntity;
@Service("cartService")
public class CartServiceImpl extends BaseServiceImpl<CartEntity> implements ICartService {
	public CartServiceImpl(){
		super();
		this.tableName="CartEntity";
		this.currentClass=CartEntity.class;
		this.initOK();
	}

	public List<CartEntity> getCartProductListByUserId(Long userId) {
		if(userId==null)
			return null;
		List<CartEntity> lst= getList("from "+getTableName()+ " where accountId="+userId,null);
		return lst;
	}

	public void clearCart(Long currentLoginedUserId) {
		List<CartEntity> lst=getCartProductListByUserId(currentLoginedUserId);
		if(lst==null)
			return;
		else{
			String ids="";
			for(CartEntity c:lst){
				ids+=c.getId()+",";
			}
			if(ids.length()>=1){
				ids=ids.substring(0,ids.length()-1);
				this.batchDel(ids);
			}
		}
	}

	
}
