package com.ibpd.shopping.service.product;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.ProductAttrDefineEntity;
@Service("productAttrDefineService")
public class ProductAttrDefineServiceImpl extends BaseServiceImpl<ProductAttrDefineEntity> implements IProductAttrDefineService {
	public ProductAttrDefineServiceImpl(){
		super();
		this.tableName="ProductAttrDefineEntity";
		this.currentClass=ProductAttrDefineEntity.class;
		this.initOK();
	}
	public static final Integer Product_Attr_Define_Params=0;
	public static final Integer Product_Attr_Define_Attrib=1;
	public List<ProductAttrDefineEntity> getAttributeListByCatalogId(Long id) {
		return getListByCatalogId(id,Product_Attr_Define_Attrib);
	}

	public List<ProductAttrDefineEntity> getListByCatalogId(Long id,
			Integer type) {
		return getList("from "+getTableName()+" where defClass="+type.toString()+" and catalogId="+id,null);
	}

	public List<ProductAttrDefineEntity> getParamListByCatalogId(Long id) {
		return getListByCatalogId(id,Product_Attr_Define_Params);
	}
	public ProductAttrDefineEntity getParentAttribute(Long pid) {
		String hql="from "+getTableName()+" where id="+pid;
		List<ProductAttrDefineEntity> al=this.getList(hql, null);
		if(al!=null && al.size()>0)
			return al.get(0);
		else
			return null;
	}

}
