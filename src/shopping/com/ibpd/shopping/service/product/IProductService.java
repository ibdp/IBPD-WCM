package com.ibpd.shopping.service.product;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.assist.ExtProductEntity;
import com.ibpd.shopping.entity.ProductEntity;

public interface IProductService extends IBaseService<ProductEntity> {
	List<ProductEntity> getListByIds(String[] ids);
	List<ProductEntity> getListByCatalogId(Long catalogId,Integer pageSize,Integer pageIndex,String queryString,String orderType);
	Long getRowCount(Long catalogId,String queryString);
	List<ExtProductEntity> getExtListByCatalogId(Long catalogId,Integer pageSize,Integer pageIndex,String queryString,String orderType);
	ExtProductEntity getProductInfo(Long productId);
	List<ProductEntity> getAboutProductList(Long productId);
	List<ProductEntity> getListByTenantId(Long tenantId,Integer pageSize);
}
