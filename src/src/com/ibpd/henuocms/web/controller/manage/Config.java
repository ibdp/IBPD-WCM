package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.DaoImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.entity.ConfigEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.service.config.ConfigServiceImpl;
import com.ibpd.henuocms.service.config.IConfigService;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;

@Controller
public class Config extends BaseController {
	private IConfigService configService=null;
	private IPageTemplateService pageTemplateService=null;//完成页面模板制作后放开
	private Map<String,String> cNameMap=null;
	private Map<String,String> cValMap=null;
	private Map<String,String> cRemMap=null;
	private List<PageTemplateEntity> pageTemplateList=null;//完成页面模板制作后放开
	@RequestMapping("manage/config/index.do")
	public String index(Model model,HttpServletRequest req) throws IOException{
		configService=(IConfigService) ((configService==null)?ServiceProxyFactory.getServiceProxy(ConfigServiceImpl.class):configService);
		configService.setDao(new DaoImpl());
		//完成页面模板制作后放开
		pageTemplateService=((pageTemplateService==null)?(IPageTemplateService)ServiceProxyFactory.getServiceProxy(PageTemplateServiceImpl.class):pageTemplateService);
		pageTemplateService.setDao(new DaoImpl());
		List<ConfigEntity> configList=configService.getList("from "+configService.getTableName(),null, "group asc,id asc", 100, 1);
		cNameMap=new HashMap<String,String>();
		cValMap=new HashMap<String,String>();
		cRemMap=new HashMap<String,String>();
		convertToMap(configList);
		pageTemplateList=pageTemplateService.getList();//完成页面模板制作后放开
		IbpdLogger.getLogger(this.getClass()).info("pageTemplateList.size="+pageTemplateList.size());
		model.addAttribute("cNameMap", cNameMap);
		model.addAttribute("cValMap", cValMap);
		model.addAttribute("cRemMap", cRemMap);
		model.addAttribute("pageTemplateList", pageTemplateList);//完成页面模板制作后放开
		return "manage/config/index";
	}	
	@RequestMapping("manage/config/doSave.do")
	public String doSave(Long id,String val) throws IOException{
		configService=(IConfigService) ((configService==null)?ServiceProxyFactory.getServiceProxy(ConfigServiceImpl.class):configService);
		configService.setDao(new DaoImpl());
		ConfigEntity t=configService.getEntityById(id);
		if(t!=null){
			t.setConfValue(val);
			configService.saveEntity(t);	
		}else{
			//还没考虑好怎么捕获异常
			return ERROR_PAGE;
		}
		return "redirect:/manage/config/index.do";
	}
	private void convertToMap(List<ConfigEntity> configList){
		if(configList!=null){
			if(configList.size()==0)
				return;
			else{
				Map<String,String> cMap=new HashMap<String,String>();
				for(Integer i=0;i<configList.size();i++){
					cNameMap.put(configList.get(i).getConfKey(), configList.get(i).getConfName());
					cValMap.put(configList.get(i).getConfKey(), configList.get(i).getConfValue());
					cRemMap.put(configList.get(i).getConfKey(), configList.get(i).getRemark());
				}
				
			}
		}else{
			return ;
		}
	}

}
