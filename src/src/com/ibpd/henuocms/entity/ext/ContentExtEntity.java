package com.ibpd.henuocms.entity.ext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.entity.ContentAttrEntity;
import com.ibpd.henuocms.entity.ContentEntity;
/**
 * 内容实体类的扩展类 跟assist下的似乎重叠了,但两者 实现的功能是不一样的，完了看看能否合并
 * @author mg by qq:349070443
 * 
 */
public class ContentExtEntity extends ContentEntity{

	private ContentAttrEntity attr;
	private ContentEntity content;


	public void setAttr(ContentAttrEntity attr) {
		this.attr = attr;
	}

	public ContentAttrEntity getAttr() {
		return attr;
	}

	public ContentEntity getContent() {
		return content;
	}
	public void setContent(ContentEntity content) {
		this.content = content;
		if(content!=null){
		try {
			setValue(content);
			IbpdLogger.getLogger(this.getClass()).info(this.toString());
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		}
	}

	private void setValue(ContentEntity content) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method[] mts=content.getClass().getMethods();
		for(Method mt:mts){
			String  mtName=mt.getName();
			if(mtName.substring(0,3).equals("get")){
				String tName="set"+mtName.substring(3);
				if(existMethod(tName)){
					Method mthod=this.getClass().getMethod(tName, mt.getReturnType());
					mthod.invoke(this, mt.invoke(content, null));
				}
			}
		}
	}
	private Boolean existMethod(String mtName){
		Method[] mt=this.getClass().getMethods();
		for(Method m:mt){
			if(m.getName().equals(mtName)){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "ContentExtEntity [getAboutKeyword()=" + getAboutKeyword()
				+ ", getAuthor()=" + getAuthor() + ", getCommentCount()="
				+ getCommentCount() + ", getCreateDate()=" + getCreateDate()
				+ ", getCustom1()=" + getCustom1() + ", getCustom10()="
				+ getCustom10() + ", getCustom11()=" + getCustom11()
				+ ", getCustom12()=" + getCustom12() + ", getCustom13()="
				+ getCustom13() + ", getCustom14()=" + getCustom14()
				+ ", getCustom15()=" + getCustom15() + ", getCustom16()="
				+ getCustom16() + ", getCustom17()=" + getCustom17()
				+ ", getCustom18()=" + getCustom18() + ", getCustom19()="
				+ getCustom19() + ", getCustom2()=" + getCustom2()
				+ ", getCustom20()=" + getCustom20() + ", getCustom3()="
				+ getCustom3() + ", getCustom4()=" + getCustom4()
				+ ", getCustom5()=" + getCustom5() + ", getCustom6()="
				+ getCustom6() + ", getCustom7()=" + getCustom7()
				+ ", getCustom8()=" + getCustom8() + ", getCustom9()="
				+ getCustom9() + ", getDeleted()=" + getDeleted()
				+ ", getDescription()=" + getDescription()
				+ ", getEnteringUser()=" + getEnteringUser()
				+ ", getExistPreview()=" + getExistPreview()
				+ ", getGoodCount()=" + getGoodCount() + ", getGroup()="
				+ getGroup() + ", getHits()=" + getHits() + ", getId()="
				+ getId() + ", getIp()=" + getIp() + ", getIsParson()="
				+ getIsParson() + ", getIsRecommend()=" + getIsRecommend()
				+ ", getIsTop()=" + getIsTop() + ", getLastUpdateDate()="
				+ getLastUpdateDate() + ", getNodeId()=" + getNodeId()
				+ ", getNodeIdPath()=" + getNodeIdPath()
				+ ", getNormalCount()=" + getNormalCount() + ", getOrder()="
				+ getOrder() + ", getPassedTime()=" + getPassedTime()
				+ ", getPassedUser()=" + getPassedUser() + ", getPreviewUrl()="
				+ getPreviewUrl() + ", getRedirectUrl()=" + getRedirectUrl()
				+ ", getSource()=" + getSource() + ", getState()=" + getState()
				+ ", getStateMessage()=" + getStateMessage()
				+ ", getSubSiteId()=" + getSubSiteId() + ", getText()="
				+ getText() + ", getTextKeywords()=" + getTextKeywords()
				+ ", getTitle()=" + getTitle() + ", getUrl()=" + getUrl()
				+ ", getUrlSize()=" + getUrlSize() + ", getUrlType()="
				+ getUrlType() + ", getWrongCount()=" + getWrongCount()
				+ ", hashCode()=" + hashCode() + ", getClass()=" + getClass()
				+ ", toString()=" + super.toString() + "]";
	}
}
